import math
import os
from PIL import Image
from random import randint

def join_path(a, b):
    return os.path.join(a, b)

def warn_not_file(name, path):
    if os.path.isfile(path):
        return False

    print("Warning: '" + name + "' is not a file.")
    return True

def get_size_lim(dir_name, is_min):
    lim_size, lim_names = None, None
    for name in os.listdir(dir_name):
        path = join_path(dir_name, name)
        if warn_not_file(name, path):
            continue

        im = Image.open(path)
        size = im.size
        if lim_size is None:
            lim_size = list(size)
            lim_names = [name, name]
        else:
            for i in range(len(size)):
                replace = size[i] < lim_size[i] if is_min else size[i] > lim_size[i]
                if replace:
                    lim_size[i] = size[i]
                    lim_names[i] = name
    return lim_size, lim_names

def get_min_size(dir_name):
    return get_size_lim(dir_name, True)

def get_max_size(dir_name):
    return get_size_lim(dir_name, False)

def print_min_size(dir_name):
    min_size, min_names = get_min_size(dir_name)
    print(min_size)
    print(min_names)

def get_centered_interval(cur, lim):
    if cur <= lim:
        return 0, cur - 1

    half = math.floor(lim / 2)
    odd  = lim % 2

    center = math.floor(cur / 2)
    first = center - half # `[i for i in range(first, center)]` has `half` elements.
    last  = center + (half + odd) - 1
    return first, last

def crop_dir(dir_name, max_size):
    for name in os.listdir(dir_name):
        path = join_path(dir_name, name)
        if warn_not_file(name, path):
            continue

        im = Image.open(path)
        size = im.size
        if size[0] < max_size[0] and size[1] < max_size[1]:
            print("Warning: Image '" + name + "' is smaller than crop size.")
            print(size)
            print(max_size)
            continue

        intervals = tuple(get_centered_interval(size[i], max_size[i]) for i in range(len(size)))
        box = (intervals[0][0], intervals[1][0], intervals[0][1] + 1, intervals[1][1] + 1)
        im = im.crop(box)
        im.save(path, im.format) # Pass `im.format` in case it doesn't match the file extension.
                                 # (Yes, it's terrible that that can happen. I agree.)

def convert_to_extension(dir_name):
    for name in os.listdir(dir_name):
        path = join_path(dir_name, name)
        im = Image.open(path)
        im.save(path)

def file_num_in_dir(dir_name):
    num = 0
    for name in os.listdir(dir_name):
        num += os.path.isfile(join_path(dir_name, name))
    return num

def augment_dir(dir_name, augment_num_per_file):
    file_num = file_num_in_dir(dir_name)
    for name in os.listdir(dir_name):
        path = join_path(dir_name, name)
        if warn_not_file(name, path):
            continue

        im = Image.open(path)
        size = im.size
        max_crop = 5
        small = 2 * max_crop
        if size[0] <= small or size[1] <= small:
            print("Warning: Small image")
            print(size)
            print(max_crop)
            continue

        for i in range(augment_num_per_file):
            c = tuple(randint(0, max_crop) for j in range(4))
            box = (c[0], c[1], size[0] - c[2], size[1] - c[3])
            out = im.crop(box)
            out_name = str(file_num).zfill(6) + '.jpg'
            out_path = join_path(dir_name, out_name)
            out.save(out_path, im.format)
            file_num += 1

def sort_dir(dir_name):
    i = 1
    for name in sorted(os.listdir(dir_name)):
        path = join_path(dir_name, name)
        if warn_not_file(name, path):
            continue

        new_name = str(i).zfill(6) + '.jpg'
        new_path = join_path(dir_name, new_name)
        os.rename(path, new_path)
        i += 1

def parse_args(a):
    assert(len(a) > 0)

    if len(a) == 1:
        print("First arg is dir name.")
        print("Second arg:")
        print('crop',          'c', '(width)', '(height)')
        print('fix extension', 'e')
        print('augment',       'a', '(number of new files for each existing file)')
        print('sort',          's', '--> (consecutive integer).jpg filenames')
        print('min size',      'm')
        print('max size',      '(anything else or nothing)')
        return

    if len(a) == 2:
        print(get_size_lim(a[1], False))
        return

    if a[2] == 'c':
        crop_dir(a[1], (int(a[3]), int(a[4])))
    elif a[2] == 'e':
        convert_to_extension(a[1])
    elif a[2] == 'a':
        augment_dir(a[1], int(a[3]))
    elif a[2] == 's':
        sort_dir(a[1])
    else:
        print(get_size_lim(a[1], a[2] == 'm'))

# ---

import sys

parse_args(sys.argv)