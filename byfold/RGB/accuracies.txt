Finetuned iter 4000 (batch size 4):
0.50386
0.50099
0.50172
0.50688
0.47328
- (Didn't calculate test accuracy.)

Balanced fold 0 iter 4000: 0.224
Iter 14000: 0.31546

Original folds:
0.22682
0.22833

Augmented fold 0 iter 1000 (batch size 20): 0.35139
Original fold 0 iter 1000 (batch size 20): 0.48717
Iter 2000: 0.49507
Fold 1: 0.50317
Fold 2: 0.49909

Augmented fold 0 iter 3000: 0.34612
Original folds:
0.49704
0.49049
