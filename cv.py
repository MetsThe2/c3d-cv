import shutil
import os, errno
import random
import math

c3d_path = '../C3D-v1.0'

# ---

def copy_file(dst_path, src_path):
    shutil.copyfile(src_path, dst_path)

def copy_frames(fold_label_frames, fold_dirs, label_dirs): # REM (unnecessary fn)
    fold_labels = []
    for fold in range(len(fold_dirs)):
        fold_labels.append([])
    for label in range(len(label_dirs)):
        for fold in range(len(fold_dirs)):
            frame = fold_label_frames[fold][label]
            src_dir = label_dirs[label]
            src_path = join_path(src_dir, get_frame_name(frame))
            fold_copied_num = len(fold_labels[fold])
            dst_dir = fold_dirs[fold]
            dst_path = join_path(dst_dir, get_frame_name(fold_copied_num))
            copy_file(dst_path, src_path)
            fold_labels[fold].append(label)
    return fold_labels

def join_path(a, b):
    return os.path.join(a, b)

def make_dir(dir_name):
    try:
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
    except OSError as e:
        if e.errno != errno.EEXIST:
            raise

def dir_num_in_dir(dir_name):
    num = 0
    for name in os.listdir(dir_name):
        num += not os.path.isfile(join_path(dir_name, name))
    return num

def file_num_in_dir(dir_name):
    num = 0
    for name in os.listdir(dir_name):
        num += os.path.isfile(join_path(dir_name, name))
    return num

def rand_elem(lst):
    return random.choice(lst)

# ---

def get_frame_str(frame):
    return str(frame + 1)

def get_frame_name(frame):
    digits = get_frame_str(frame).zfill(6)
    if len(digits) > 6:
        print('Warning: (frame + 1) is too big to fit in six digits.')
        print('Digits: ', digits)
    return digits + '.jpg'

def check_frame_nums(fold_label_frames, label_frame_nums, frame_step):
    for label in range(len(label_frame_nums)):
        s = 0
        for fold in range(len(fold_label_frames)):
            s += len(fold_label_frames[fold][label])
        print('Label: ', label)
        print('Total label frames:', label_frame_nums[label])
        print('Approx used label frames:', s * frame_step) # Should be close to total.

def write_fold_label_frames(fold_label_frames):
    fold_strs = []
    for label_frames in fold_label_frames:
        label_strs = []
        for frames in label_frames:
            frame_strs = []
            for frame in frames:
                frame_strs.append(get_frame_str(frame))
            label_strs.append(','.join(frame_strs))
        fold_strs.append('\n'.join(label_strs))
    s = '\n---\n'.join(fold_strs)
    with open('fold_label_frames.txt', 'w') as f:
        f.write(s)

def get_fold_io_strs(fold_dir, label_dirs, label_frames):
    input_lines, output_lines = [], []
    for label in range(len(label_frames)):
        frames, label_dir = label_frames[label], label_dirs[label]
        for frame in frames:
            abs_label_dir = os.path.abspath(label_dir)
            input_line = ' '.join((abs_label_dir, get_frame_str(frame), str(label)))
            output_name = get_frame_str(len(output_lines)).zfill(6)
            output_line = join_path(os.path.abspath(fold_dir), output_name)
            input_lines.append(input_line)
            output_lines.append(output_line)
    input_str, output_str = '\n'.join(input_lines), '\n'.join(output_lines)
    return input_str, output_str

def get_fold_name(fold, fold_num, by_fold_dir):
    return 'test' if fold == fold_num - 1 else 'fold' + str(fold) 

def get_fold_dir(fold, fold_num, by_fold_dir):
    return join_path(by_fold_dir, get_fold_name(fold, fold_num, by_fold_dir))

def get_input_path(fold, fold_num, by_fold_dir):
    fold_dir = get_fold_dir(fold, fold_num, by_fold_dir)
    return join_path(fold_dir, 'input_list.txt')

def get_output_path(fold, fold_num, by_fold_dir):
    fold_dir = get_fold_dir(fold, fold_num, by_fold_dir)
    return join_path(fold_dir, 'output_prefix.txt')

def write_folds(fold_num, frame_step, by_label_dir, by_fold_dir):

    label_num = dir_num_in_dir(by_label_dir)
    label_dirs, label_frame_nums = [], []
    for i in range(label_num):
        label_dirs.append(join_path(by_label_dir, 'label' + str(i)))
        label_frame_nums.append(file_num_in_dir(label_dirs[i]))
    print('Number of labels: ', label_num)
    print('Number of frames for each label:')
    print(label_frame_nums)

    make_dir(by_fold_dir)
    fold_label_frames = []
    for fold in range(fold_num):
        make_dir(get_fold_dir(fold, fold_num, by_fold_dir))
        fold_label_frames.append([])
        for label in range(label_num):
            fold_label_frames[fold].append([])

    min_label_nums = []
    for frame_num in label_frame_nums:
        min_label_nums.append(math.floor(frame_num / (fold_num * frame_step)))

    all_folds = [fold for fold in range(fold_num)]
    for label in range(label_num):
        partial_folds = all_folds[:]

        always_same_frame_step = True # Set to False if C3D supports that.
        use_frame_num = label_frame_nums[label]
        if always_same_frame_step:
            # Ignore some frames so use_frame_num is a multiple of frame_step.
            use_frame_num = use_frame_num - (use_frame_num % frame_step)

        for frame in range(0, use_frame_num, frame_step):
            fold = rand_elem(partial_folds if len(partial_folds) > 0 else all_folds)
            frames = fold_label_frames[fold][label]
            frames.append(frame)
            fold_label_frame_num = len(frames)
            if fold_label_frame_num >= min_label_nums[label]:
                if fold not in partial_folds:
                    partial_folds.remove(fold)

    check_frame_nums(fold_label_frames, label_frame_nums, frame_step)
    write_fold_label_frames(fold_label_frames)
    for fold in range(fold_num):
        fold_dir = get_fold_dir(fold, fold_num, by_fold_dir)
        label_frames = fold_label_frames[fold]
        istr, ostr = get_fold_io_strs(fold_dir, label_dirs, label_frames)
        input_path = get_input_path(fold, fold_num, by_fold_dir)
        output_path = get_output_path(fold, fold_num, by_fold_dir)
        with open(input_path, 'w') as f:
            f.write(istr)
        with open(output_path, 'w') as f:
            f.write(ostr)

# ---

import subprocess

def get_labels(input_path, label_num):
    input_str = None
    with open(input_path) as f:
        input_str = f.read()
    lines = input_str.split('\n')
    labels = []
    for line in lines:
        a = line.split(' ')
        if len(a) != 3:
            print("Warning: Strange line with " + str(len(a)) + " things.")
            print(line)
            continue

        label = int(a[-1])
        if not isinstance(label, int) or label < 0 or label >= label_num:
            print("Warning: strange label")
            print(a[-1])
            continue

        labels.append(label)
    return labels

def execute_sh(sh_path):
    args = ('sh', sh_path)
    return subprocess.call(args)

def get_sh_output(sh_path):
    args = ('sh', sh_path)
    return subprocess.check_output(args, stderr=subprocess.STDOUT, universal_newlines=True)

# TODO: execute_sh_str / execute_script?

def get_model_name(model_info, fold):
    return model_info.prefix + '_fold_' + str(fold) + '_iter_' + str(model_info.iter)
        
def get_model_dir(model_info):
    return join_path('model', model_info.channel)

def get_model_path(model_info, fold):
    return join_path(get_model_dir(model_info), get_model_name(model_info, fold))

def get_outputs(fold, fold_num, by_fold_dir, output_num, label_num, model_info):
    debug_output = False
    if debug_output:
        return [0] * output_num

    with open('test.sh', 'w') as f:
        prefix = 'GLOG_logtostderr=1 ' + join_path(c3d_path, 'build/tools/test_net.bin') + ' test.prototxt'
        script = ' '.join((prefix, get_model_path(model_info, fold), str(output_num), 'GPU 0'))
        f.write(script)
    s = get_sh_output('test.sh')

    in_prev_line = 'Running ' + str(output_num)
    first = s.find(in_prev_line)
    if first < 0:
        print('No outputs?')
        return []

    outputs = []
    s = s[first:]
    lines = s.split('\n')
    for i in range(1, output_num + 1):
        line = lines[i]
        if 'Batch' not in line:
            print('Missing Batch')
            print(line)
            continue

        prefix = 'accuracy: '
        prefix_index = line.find(prefix)
        if prefix_index < 0:
            print('Missing ' + prefix)
            print(line)
            continue

        output_index = prefix_index + len(prefix)
        if output_index >= len(line):
            print('Missing output')
            print(line)
            continue
        
        output_str = line[output_index:].strip()
        outputs.append(int(output_str))
    return outputs

# ---

def test_on_fold(fold, fold_num, by_fold_dir, label_num, model_info):
    print('Testing...')
    copy_file('test_list.txt', get_input_path(fold, fold_num, by_fold_dir))
    labels = get_labels('test_list.txt', label_num)

    max_test_size = None
    if max_test_size is not None and len(labels) > max_test_size:
        del labels[max_test_size - len(labels) : ]

    outputs = get_outputs(fold, fold_num, by_fold_dir, len(labels), label_num, model_info)
    avg_output = sum(outputs) / len(outputs)
    print('Fold labels:')
    print(labels)
    print('Fold outputs:')
    print(outputs)
    
    label_output_nums = [] # Confusion matrix
    for label in range(label_num):
        label_output_nums.append([0] * label_num)
    
    for i in range(len(outputs)):
        label_output_nums[labels[i]][outputs[i]] += 1
    return label_output_nums, avg_output

def write_train_list(folds, fold_num, by_fold_dir):
    input_strs = []
    for fold in folds:
        input_path = get_input_path(fold, fold_num, by_fold_dir)
        with open(input_path, 'r') as f:
            input_strs.append(f.read())
    
    # TODO: Why must the input strings be written in reverse order?
    with open('train_list.txt', 'w') as f:
        n = len(input_strs)
        for i in range(n - 1, -1, -1):
            f.write(input_strs[i])
            if i > 0:
                f.write('\n')

def write_volume_mean(model_info):
    prefix = 'GLOG_logtostderr=1 ' + join_path(c3d_path, 'build/tools/compute_volume_mean_from_list.bin')
    input_path = 'train_list.txt'
    frame_step = model_info.frame_step
    width = 201
    height = 201 # TODO: Or is the first one height and the second one width?
    something = 1 # TODO: What is this?
    mean_path = 'volume_mean.binaryproto'
    sample_rate = 10 # Every tenth pixel is used.
    with open('volume.sh', 'w') as f:
        parts = (prefix, input_path, frame_step, width, height, something, mean_path, sample_rate)
        f.write(' '.join(tuple(str(part) for part in parts)))
    execute_sh('volume.sh')

def train_on_folds(folds, fold_num, by_fold_dir, pretrained, model_path, model_info):
    print('Training...')

    test_fold = fold_num - 1
    if test_fold in folds:
        print("Warning: Don't train on test fold.")
        folds.remove(test_fold)

    write_train_list(folds, fold_num, by_fold_dir)
    write_volume_mean(model_info)
   
    with open('finetune.sh', 'w') as f:
        prefix = 'GLOG_logtostderr=1 ' + join_path(c3d_path, 'build/tools/finetune_net.bin') + ' solver.prototxt '
        f.write(prefix + pretrained)
    execute_sh('finetune.sh')
    
    snapshot_path = join_path('model', 'finetuned_iter_' + str(model_info.iter))
    copy_file(model_path, snapshot_path)

def validate_on_fold(validation_fold, fold_num, by_fold_dir, pretrained, label_num, model_info):
    train_folds = []
    max_train_fold_num = fold_num - 1
    for fold in range(max_train_fold_num):
        if fold != validation_fold:
            train_folds.append(fold)
    model_path = get_model_path(model_info, validation_fold)
    train_on_folds(train_folds, fold_num, by_fold_dir, pretrained, model_path, model_info)
    return test_on_fold(validation_fold, fold_num, by_fold_dir, label_num, model_info)
def accuracy_from_confusion_mat(confusion_mat):
    accurate_num, total_num = 0, 0
    for label in range(len(confusion_mat)):
        for output in range(len(confusion_mat[label])):
            output_num = confusion_mat[label][output]
            total_num += output_num
            accurate_num += (label == output) * output_num
    return accurate_num / total_num, total_num

def write_train_prototxt(frame_step): # TODO
    pass

def write_test_prototxt(frame_step): # TODO
    pass

def write_solver_prototxt(model_iter): # TODO
    pass

def write_prototxts(frame_step, model_iter):
    write_train_prototxt(frame_step)
    write_test_prototxt(frame_step)
    write_solver_prototxt(model_iter)

# ---

from collections import namedtuple

frame_step   = 16
# model_iter   = 1000
model_iter   = 900
# model_prefix = 'scaled'
model_prefix = ''
pretrained = 'model/conv3d_deepnetA_sport1m_iter_1900000'

channel_name = 'D'
label_num    = 5
fold_num     = 6
by_label_dir = join_path('bylabel', channel_name)
by_fold_dir  = join_path('byfold', channel_name)

ModelInfo = namedtuple('ModelInfo', ('iter', 'prefix', 'channel', 'frame_step'))

def init():
    print("Channels: ", channel_name)
    make_dir('byfold')
    write_folds(fold_num, frame_step, by_label_dir, by_fold_dir)

def run_fold(fold, test_only):
    assert(fold < fold_num)
    write_prototxts(frame_step, model_iter)
    model_info = ModelInfo(iter = model_iter, prefix = model_prefix,
                           channel = channel_name, frame_step = frame_step)

    confusion_mat, avg_output = None, None
    if test_only:
        confusion_mat, avg_output = test_on_fold(fold, fold_num, by_fold_dir,
                                                 label_num, model_info)
    else:
        confusion_mat, avg_output = validate_on_fold(fold, fold_num, by_fold_dir,
                                                     pretrained, label_num, model_info)
    
    accuracy, output_num = accuracy_from_confusion_mat(confusion_mat)
    print('Accuracy: ', accuracy)
    print('Test size: ', output_num)
    print('Avg output: ', avg_output)

def cv(test_only):
    for fold in range(fold_num - 1):
        print('Fold: ', fold)
        run_fold(fold, test_only)

def parse_args(args):
    n = len(args)
    if n < 2:
        print('i\t\t\tinit')
        print('c\t\t\tcv')
        print('(fold) [t]\t\tvalidate or test')
        return

    test_only = n >= 3 and args[2] == 't'
    a1 = args[1]
    if a1 == 'i':
        init()
    elif a1 == 'c':
        cv(test_only)
    else:
        run_fold(int(a1), test_only)

# ---

import sys

random.seed(1234)
parse_args(sys.argv)
